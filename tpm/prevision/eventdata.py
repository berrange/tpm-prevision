# SPDX-License-Identifier: LGPL-2.1-or-later

import abc
import hashlib
from uuid import UUID

from .pe import PEImage
from .gpt import GPTHeader
from .optionrom import OptionROMFile


class EventData(abc.ABC):

    @abc.abstractmethod
    def digest(self):
        pass

class EventDataPreHashed(EventData):

    def __init__(self, value):
        self.value = value

    def digest(self):
        return bytes.fromhex(self.value)

class EventDataHashable(EventData, abc.ABC):

    @abc.abstractmethod
    def data(self):
        pass

    def digest(self):
        return hashlib.sha256(self.data()).digest()

class EventDataString(EventDataHashable):
    def __init__(self, value, encoding):
        self.value = value
        self.encoding = encoding

    def data(self):
        return self.value.encode(self.encoding)

    @staticmethod
    def from_file(path, encoding):
        with open(path, "rb") as fh:
            return EventDataString(fh.read(), encoding)

class EventDataRaw(EventDataHashable):
    def __init__(self, value):
        self.value = value

    def data(self):
        return self.value

    @staticmethod
    def from_file(path):
        with open(path, "rb") as fh:
            return EventDataRaw(fh.read())

class EventDataHex(EventDataHashable):
    def __init__(self, value):
        self.value = value

    def data(self):
        return bytes.fromhex(self.value)

    @staticmethod
    def from_file(path):
        with open(path, "rb") as fh:
            return EventDataHex(fh.read())

class EventDataInt(EventDataHashable):
    def __init__(self, value, length, endian='little'):
        self.value = value
        self.length = length
        self.endian = endian

    def data(self):
        return self.value.to_bytes(self.length, self.endian)

class EventDataOptionROMAuthenticode(EventData):
    def __init__(self, path):
        self.path = path

    def digest(self):
        romfile = OptionROMFile(self.path)
        for rom in romfile.roms:
            if rom.isEFI():
                p = rom.raw_payload()
                pe = PEImage(data=p)
                return pe.get_authenticode_hash()

        raise Exception("Not an EFI option ROM")

class EventDataPEImageAuthenticode(EventData):
    def __init__(self, path=None, data=None):
        self.path = path
        self.data = data

    def digest(self):
        pe = PEImage(path=self.path, data=self.data)
        return pe.get_authenticode_hash()

class EventDataAzureVMIdentifier(EventDataHashable):
    def __init__(self, vmid):
        self.vmid = vmid

    def data(self):
        return ("UUID: %s" % str(self.vmid).upper()).encode("ascii")

class EventDataBootOrder(EventDataHashable):
    def __init__(self, indexes):
        self.indexes = indexes

    def data(self):
        indexes = bytes([])
        for index in self.indexes:
            indexes = indexes + index.to_bytes(2, 'little')
        return indexes

class EventDataBootEntry(EventDataHashable):
    def __init__(self, bootentry):
        self.bootentry = bootentry

    def data(self):
        return self.bootentry.encode()

class EventDataVariable(EventDataHashable):

    GUID_GLOBAL_VARIABLE = UUID("8be4df61-93ca-11d2-aa0d-00e098032b8c")
    GUID_SECURITY_DATABASE = UUID("d719b2cb-3d3a-4596-a3bc-dad00e67656f")
    GUID_SHIM_LOCK = UUID("605dab50-e046-4300-abb6-3dd810dd8b23")

    def __init__(self, guid, name, value):
        self.guid = guid
        self.name = name
        self.value = value

    def data(self):
        d = self.guid.bytes_le
        d += len(self.name).to_bytes(8, 'little')
        d += len(self.value).to_bytes(8, 'little')
        d += self.name.encode('utf-16-le')
        d += self.value
        return d

    @staticmethod
    def from_cert_list_file(guid, name, path):
        with open(path, "rb") as fh:
            return EventDataVariable(guid, name, fh.read())

    @staticmethod
    def from_cert_file(guid, name, path):
        with open(path, "rb") as fh:
            return EventDataVariable(guid, name, fh.read())

    @staticmethod
    def from_guid_cert_file(guid, name, cert_guid, path):
        with open(path, "rb") as fh:
            return EventDataVariable(guid, name, cert_guid.bytes_le + fh.read())

class EventDataGPT(EventDataHashable):

    def __init__(self, gpt):
        self.gpt = gpt

    def data(self):
        return self.gpt.pack()

class EventDataPEImageSection(EventDataHashable):

    def __init__(self, peimage, section, strencode=None):
        self.peimage = peimage
        self.section = section
        self.strencode = strencode

    def data(self):
        data = self.peimage.get_section_data(self.section)
        if self.strencode is not None:
            s = data.decode("ascii")
            data = s.encode(self.strencode)
        return data
