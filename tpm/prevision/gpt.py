# SPDX-License-Identifier: LGPL-2.1-or-later

import binascii
from uuid import UUID

class GPTEntry:

    def __init__(self, typeGUID, guid, lbaStart, lbaEnd, attr, name):
        self.typeGUID = typeGUID
        self.guid = guid
        self.lbaStart = lbaStart
        self.lbaEnd = lbaEnd
        self.attr = attr
        self.name = name

    def __repr__(self):
        if self.isActive():
            return "<gptpart:type=%s,guid=%s,lba-start=%x,lba-end=%x,attr=%x,name=%s>" % (
                self.typeGUID, self.guid, self.lbaStart, self.lbaEnd, self.attr, self.name
            )
        else:
            return "<gptpart:inactive>"

    def isActive(self):
        return self.guid.bytes != bytes([0] * 16)

    def pack(self):
        padname = self.name.encode('utf-16-le')
        padname += bytes([0] * (72 - len(padname)))
        return (self.typeGUID.bytes_le +
                self.guid.bytes_le +
                self.lbaStart.to_bytes(8, 'little') +
                self.lbaEnd.to_bytes(8, 'little') +
                self.attr.to_bytes(8, 'little') +
                padname)

class GPTHeader:

    SIG = bytes([0x45, 0x46, 0x49, 0x20, 0x50, 0x41, 0x52, 0x54])
    REV_1_0 = bytes([0x00, 0x00, 0x01, 0x00])
    HDR_LEN = 92

    def __init__(self, lba, lbaBackup, lbaFirstUsable, lbaLastUsable,
                 guid, lbaEntries, entrySize, entries):
        self.lba = lba
        self.lbaBackup = lbaBackup
        self.lbaFirstUsable = lbaFirstUsable
        self.lbaLastUsable = lbaLastUsable
        self.guid = guid
        self.lbaEntries = lbaEntries
        self.entrySize = entrySize
        self.entries = entries

    def _crc(self, data):
        return binascii.crc32(data)

    def find_esp(self):
        esp = UUID("c12a7328-f81f-11d2-ba4b-00a0c93ec93b")
        for index, part in enumerate(self.entries):
            if part.typeGUID == esp:
                return index, part
        raise Exception("Cannot find EFI system partition")

    def pack(self):
        nentries = 0
        for e in self.entries:
            if e.isActive():
                nentries += 1

        # Yes, its odd, we calculate crc across the header and
        # all entries with padding, but for TPM event we only
        # pack the active entries, so the CRC won't match
        header = self._pack_header(crc=self._crc(self._pack_header()),
                                   pad=False)

        header += nentries.to_bytes(8, 'little')
        for e in self.entries:
            if e.isActive():
                header += e.pack()

        return header

    def _pack_header(self, crc=None, pad=False):
        if crc is None:
            crc = 0

        entries = bytes([]).join([
            e.pack() for e in self.entries])

        header = (self.SIG +
                  self.REV_1_0 +
                  self.HDR_LEN.to_bytes(4, 'little') +
                  crc.to_bytes(4, 'little') +
                  bytes([0] * 4) +
                  self.lba.to_bytes(8, 'little') +
                  self.lbaBackup.to_bytes(8, 'little') +
                  self.lbaFirstUsable.to_bytes(8, 'little') +
                  self.lbaLastUsable.to_bytes(8, 'little') +
                  self.guid.bytes_le +
                  self.lbaEntries.to_bytes(8, 'little') +
                  len(self.entries).to_bytes(4, 'little') +
                  self.entrySize.to_bytes(4, 'little') +
                  self._crc(entries).to_bytes(4, 'little'))
        if pad:
            header += bytes([0] * 512)

        return header

    @staticmethod
    def from_device(path):
        with open(path, "rb") as fp:
            # SKIP the MBR
            fp.seek(512)

            sig = fp.read(8)
            if sig != GPTHeader.SIG:
                raise Exception("Not a GPT partition table")
            rev = fp.read(4)
            if rev != GPTHeader.REV_1_0:
                raise Exception("Not a GPT version != 1.0 ")
            size = int.from_bytes(fp.read(4), 'little')
            if size != GPTHeader.HDR_LEN:
                raise Exception("GPT header %d bytes not %d" % (size, GPTHeader.HDR_LEN))

            fp.read(4) # CRC
            fp.read(4) # reserved
            lba = int.from_bytes(fp.read(8), 'little')
            lbaBackup = int.from_bytes(fp.read(8), 'little')
            lbaFirstUsable = int.from_bytes(fp.read(8), 'little')
            lbaLastUsable = int.from_bytes(fp.read(8), 'little')
            guid = UUID(bytes_le=fp.read(16))
            lbaEntries = int.from_bytes(fp.read(8), 'little')
            numEntries = int.from_bytes(fp.read(4), 'little')
            entSize = int.from_bytes(fp.read(4), 'little')
            fp.read(4) # entry CRC
            fp.read(420) # pad

            fp.seek(lbaEntries*512)
            entries = []
            for i in range(numEntries):
                typeGUID = UUID(bytes_le=fp.read(16))
                partGUID = UUID(bytes_le=fp.read(16))
                lbaFirst = int.from_bytes(fp.read(8), 'little')
                lbaLast = int.from_bytes(fp.read(8), 'little')
                attr = int.from_bytes(fp.read(8), 'little')
                name = fp.read(72).decode('utf-16-le').rstrip('\0')
                entries.append(
                    GPTEntry(typeGUID, partGUID, lbaFirst, lbaLast, attr, name))

            header = GPTHeader(lba, lbaBackup, lbaFirstUsable, lbaLastUsable,
                               guid, lbaEntries, entSize, entries)
            return header
