# SPDX-License-Identifier: LGPL-2.1-or-later

# https://github.com/theopolis/uefi-firmware-parser/
from uefi_firmware import efi_compressor

class OptionROM:

    def __init__(self, start, end, header, pcirHeader, efiHeader):
        self.start = start
        self.end = end
        self.header = header
        self.pcirHeader = pcirHeader
        self.efiHeader = efiHeader
        self.payload = bytes([])

    def isEFI(self):
        return self.efiHeader is not None

    def load_payload(self, fh):
        start = self.start + 512
        if self.isEFI():
            start = self.start + self.efiHeader.efiImageHeaderOffset
        fh.seek(start)
        self.payload = fh.read(self.end - start)

    def raw_payload(self):
        if self.efiHeader is None:
            return self.payload
        if self.efiHeader.compressionType != OptionROMEFIHeader.COMPRESSION_TYPE_EFI:
            return self.payload

        try:
            return efi_compressor.EfiDecompress(self.payload, len(self.payload))
        except:
            try:
                return efi_compressor.TianoDecompress(self.payload, len(self.payload))
            except:
                return efi_compressor.LzmaDecompress(self.payload, len(self.payload))

    def __repr__(self):
        return "start=%d end=%d" % (self.start, self.end)


# PCI_EXPANSION_ROM_HEADER from EDK2 pci22.h
class OptionROMHeader:

    ROM_SIG = bytes([0x55, 0xaa])

    def __init__(self, pcirOffset):
        self.pcirOffset = pcirOffset

    def __repr__(self):
        return "pcirOffset=%x" % self.pcirOffset

    @staticmethod
    def load(fh):
        sig = fh.read(2)
        if sig != OptionROMEFIHeader.ROM_SIG:
            raise Exception("Invalid Option ROM header signature")

        fh.read(0x16)
        pcirOffset = int.from_bytes(fh.read(2), 'little')

        return OptionROMHeader(pcirOffset)


# PCI_DATA_STRUCTURE from EDK2 pci22.h
class OptionROMPCIRHeader:

    ROM_SIG = "PCIR".encode('ascii')

    CODE_TYPE_EFI_IMAGE = 0x03

    INDICATOR_LAST = 0x80

    def __init__(self, vendorid, deviceid, length, revision, classCode,
                 imageLength, codeRevision, codeType, indicator):
        self.vendorid = vendorid
        self.deviceid = deviceid
        self.length = length
        self.revision = revision
        self.classCode = classCode
        self.imageLength = imageLength
        self.codeRevision = codeRevision
        self.codeType = codeType
        self.indicator = indicator

    def isLast(self):
        return self.indicator == self.INDICATOR_LAST

    def __repr__(self):
        return ("vendorid=%x deviceid=%x length=%x revision=%x classCode=%s " +
                "imageLength=%x codeRevision=%x codeType=%x indicator=%x") % (
                    self.vendorid, self.deviceid, self.length, self.revision,
                    ",".join(["%x" % c for c in self.classCode]),
                    self.imageLength, self.codeRevision, self.codeType, self.indicator
                )

    @staticmethod
    def load(fh):
        sig = fh.read(4)
        if sig != OptionROMPCIRHeader.ROM_SIG:
            raise Exception("Invalid Option ROM PCIR header signature")

        vendorid = int.from_bytes(fh.read(2), 'little')
        deviceid = int.from_bytes(fh.read(2), 'little')
        fh.read(2)
        length = int.from_bytes(fh.read(2), 'little')
        revision = int.from_bytes(fh.read(1))
        classCode = [int.from_bytes(fh.read(1)) for c in range(3)]
        imageLength = int.from_bytes(fh.read(2), 'little')
        codeRevision = int.from_bytes(fh.read(2), 'little')
        codeType = int.from_bytes(fh.read(1))
        indicator = int.from_bytes(fh.read(1))
        fh.read(2)

        return OptionROMPCIRHeader(vendorid, deviceid, length, revision,
                                   classCode, imageLength, codeRevision,
                                   codeType, indicator)

# EFI_PCI_EXPANSION_ROM_HEADER from EDK2 EfiPci.h
class OptionROMEFIHeader:

    ROM_SIG = bytes([0x55, 0xaa])
    ROM_EFI_SIG = bytes([0xf1, 0x0e, 0x00, 0x00])

    COMPRESSION_TYPE_EFI = 0x1

    def __init__(self, initSize, efiSubsystem, efiMachineType,
                 compressionType, efiImageHeaderOffset, pcirOffset):
        self.initSize = initSize
        self.efiSubsystem = efiSubsystem
        self.efiMachineType = efiMachineType
        self.compressionType = compressionType
        self.efiImageHeaderOffset = efiImageHeaderOffset
        self.pcirOffset = pcirOffset

    def __repr__(self):
        return ("initSize=%x efiSubsystem=%x efiMachineType=%x " +
                "compressionType=%x efiImageHeaderOffset=%x pcirOffset=%x") % (
                    self.initSize, self.efiSubsystem, self.efiMachineType,
                    self.compressionType, self.efiImageHeaderOffset, self.pcirOffset
                )

    @staticmethod
    def load(fh):
        sig = fh.read(2)
        if sig != OptionROMEFIHeader.ROM_SIG:
            raise Exception("Invalid Option ROM header signature")

        initSize = int.from_bytes(fh.read(2), 'little')
        efiSignature = fh.read(4)
        if efiSignature != OptionROMEFIHeader.ROM_EFI_SIG:
            raise Exception("Invalid Option ROM EFI header signature")
        efiSubsystem = int.from_bytes(fh.read(2), 'little')
        efiMachineType = int.from_bytes(fh.read(2), 'little')
        compressionType = int.from_bytes(fh.read(2), 'little')
        fh.read(8)
        efiImageHeaderOffset = int.from_bytes(fh.read(2), 'little')
        pcirOffset = int.from_bytes(fh.read(2), 'little')

        return OptionROMEFIHeader(initSize, efiSubsystem, efiMachineType,
                                  compressionType, efiImageHeaderOffset, pcirOffset)


class OptionROMFile:

    def __init__(self, path):
        self.path = path
        self.roms = []

        with open(self.path, "rb") as fh:
            self.load(fh)

    def __repr__(self):
        return "path=%s roms=%s" % (
            self.path,
            ",".join(["%s" % r for r in self.roms])
            )

    def load(self, fh):
        last = 0
        offset = 0
        header = None
        pcirHeader = None
        efiHeader = None

        filelen = fh.seek(0, 2)

        while True:
            fh.seek(offset)

            # To find the end of the payload keep looking for
            # the ROM signature every 512 bytes
            sig = fh.read(2)
            if sig != OptionROMHeader.ROM_SIG:
                offset += 512
                continue

            if offset != 0:
                rom = OptionROM(last, offset, header, pcirHeader, efiHeader)
                rom.load_payload(fh)
                self.roms.append(rom)
                header = None
                last = offset

            fh.seek(offset)
            header = OptionROMHeader.load(fh)

            fh.seek(offset + header.pcirOffset)
            pcirHeader = OptionROMPCIRHeader.load(fh)

            if pcirHeader.codeType == OptionROMPCIRHeader.CODE_TYPE_EFI_IMAGE:
                # GO back and re-read the ROM header, as an EFI header
                fh.seek(offset)
                efiHeader = OptionROMEFIHeader.load(fh)

            if pcirHeader.isLast():
                rom = OptionROM(last, filelen, header, pcirHeader, efiHeader)
                rom.load_payload(fh)
                self.roms.append(rom)
                break

            offset += 512
