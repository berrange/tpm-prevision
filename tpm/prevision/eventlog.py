# SPDX-License-Identifier: LGPL-2.1-or-later

from .eventdata  import EventDataInt
from .eventdata  import EventDataString
from .eventdata  import EventDataGPT

class Event:

    def __init__(self, pcr, data):
        self.pcr = pcr
        self.data = data
        self.digestval = None

    def digest(self):
        if self.digestval is None:
            self.digestval = self.data.digest()
        return self.digestval

    def __repr__(self):
        return "%2d:%s:%s" % (self.pcr, self.__class__.__name__, self.digest().hex())

class EventSCRTM(Event):
    def __init__(self, pcr, version):
        super().__init__(pcr, EventDataInt(version, 2, 'little'))

class EventEFIPlatformFirmwareBlob(Event):
    pass

class EventEFIBootServicesDriver(Event):
    pass

class EventEFIBootServicesApplication(Event):
    pass

class EventEFIAction(Event):
    def __init__(self, pcr, strval):
        super().__init__(pcr, EventDataString(strval, 'ascii'))

class EventCompactHash(Event):
    pass

class EventSeparator(Event):
    def __init__(self, pcr, ok=True):
        # Spec allows for 0xffffffff instead of 0 on success,
        # but not seen that used (yet)
        val = 0
        if not ok:
            val = 1
        super().__init__(pcr, EventDataInt(val, 4, 'little'))

class EventEFIVariableBoot(Event):
    pass

class EventEFIVariableAuthority(Event):
    pass

class EventEFIVariableDriverConfig(Event):
    pass

class EventEFIGPTEvent(Event):
    def __init__(self, pcr, gpt):
        super().__init__(pcr, EventDataGPT(gpt))

class EventIPL(Event):
    pass

class EventEventTag(Event):
    pass

class EventLog:
    def __init__(self):
        self.events = []

    def __repr__(self):
        return "".join([str(e) + "\n" for e in self.events])

    def record(self, event):
        self.events.append(event)
