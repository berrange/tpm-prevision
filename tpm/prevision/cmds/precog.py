#!/usr/bin/python
#
# SPDX-License-Identifier: LGPL-2.1-or-later

import argparse
import logging

from ..precog import Precognitive
from ..manifest import FirmwareManifest, ImageManifest

log = logging.getLogger()

def main():
    parser = argparse.ArgumentParser("TPM precog")
    parser.add_argument("--firmware-manifest",
                        help='Path to firmware manifest YAML file')

    parser.add_argument("--image-file",
                        help='Path to image file block device')
    parser.add_argument("--image-mount",
                        help='Path to image mount point')
    parser.add_argument("--image-manifest",
                        help='Path to image manifest YAML file')

    parser.add_argument('--event-log', '-e', action='store_true',
                        help='Show event log')
    parser.add_argument('--tpm-pcrs', '-p', action='store_true',
                        help='Show TPM PCRs')
    parser.add_argument('--debug', '-d', action='store_true',
                        help='Show debug information')

    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(level="DEBUG")
        formatter = logging.Formatter("[%(levelname)s]: %(message)s")
        handler = log.handlers[0]
        handler.setFormatter(formatter)

    precog = Precognitive(FirmwareManifest(args.firmware_manifest),
                          args.image_file,
                          args.image_mount,
                          ImageManifest(args.image_manifest))
    tpm = precog.vision()

    if args.event_log:
        print(tpm.log)
    if args.tpm_pcrs or not args.event_log:
        print(tpm)
