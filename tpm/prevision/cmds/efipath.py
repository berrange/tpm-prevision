#!/usr/bin/python

import sys

from ..efiboot import EFIPath

def main():
    for arg in sys.argv[1:]:
        print("Path %s" % arg)

        components = EFIPath.decode_path(bytes.fromhex(arg))
        print("/".join([str(c) for c in components]))
