# SPDX-License-Identifier: LGPL-2.1-or-later

import hashlib
import re
import subprocess
import logging

from .eventlog import EventLog

log = logging.getLogger()

class PCR:

    def __init__(self, idx):
        self.index = idx
        self.value = bytes([0] * 32)

    def __repr__(self):
        return "%2d:%s" % (self.index, self.value.hex())

    def initialize(self, value):
        self.value = value

    def extend(self, event):
        self.value = hashlib.sha256(self.value + event.digest()).digest()

class TPM:

    def __init__(self):
        self.pcrs = [PCR(p) for p in range(24)]
        self.log = EventLog()

    def __repr__(self):
        return "".join([str(p) + "\n" for p in self.pcrs])

    def initialize(self, pcr, value):
        self.pcrs[pcr].initialize(value)

    def extend(self, event):
        self.log.record(event)
        log.debug("Extend event %s" % event)
        self.pcrs[event.pcr].extend(event)
        log.debug("New PCR %s" % self.pcrs[event.pcr])

    @staticmethod
    def from_host():
        tpm = TPM()
        proc = subprocess.run("tpm2_pcrread", capture_output=True, encoding='utf8')
        pcrre = re.compile(r'''\s*(\d+)\s*:\s*0x([A-F0-9]+)\s*''')
        for line in proc.stdout.split("\n"):
            res = pcrre.match(line)
            if res is not None:
                pcr = int(res.group(1))
                measurement = bytes.fromhex(res.group(2))

                tpm.pcrs[pcr].value = measurement
        return tpm

    def compare(self, reference, ignoreIMA=True):
        for i in range(24):
            if ignoreIMA and i == 10:
                continue
            if self.pcrs[i].value != reference.pcrs[i].value:
                print("Mismatch %d: %s != %s\n" % (
                    i, self.pcrs[i].value.hex(), reference.pcrs[i].value.hex()))
