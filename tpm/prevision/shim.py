# SPDX-License-Identifier: LGPL-2.1-or-later

from pathlib import Path

from .efiboot import EFIMediaPathHardDrive, EFIMediaPathFile, EFIPathTerminator

class ShimSBAT:

    def __init__(self):
        self.records = []

    def add_component(self, name, generation, date):
        self.records.append([name, generation, date])
        return self

    def add_default_component(self):
        return self.add_component("sbat", "1", "2021030218")

    def encode(self):
        data = bytes([])
        for rec in self.records:
            data += (",".join(rec) + "\n").encode('ascii')
        return data

class ShimBootEntry:
    def __init__(self, file, label, data):
        self.file = file
        self.label = label
        self.data = data

    def efi_path(self, gpt):
        index, part = gpt.find_esp()

        return [
            EFIMediaPathHardDrive(index + 1, part.lbaStart,
                                  (part.lbaEnd - part.lbaStart) + 1,
                                  part.guid,
                                  EFIMediaPathHardDrive.FMT_GPT,
                                  EFIMediaPathHardDrive.SIG_TYPE_GUID),
            EFIMediaPathFile("\\" + self.file.replace("/", "\\")),
            EFIPathTerminator(),
        ]

class ShimBootCSV:
    def __init__(self):
        self.entries = []

    @classmethod
    def from_file(cls, path):
        basedir = Path(path).parent
        espdir = basedir.parent.parent
        reldir = basedir.relative_to(espdir)
        csv = cls()
        with open(path, "r", encoding="utf-16-le") as fh:
            for line in fh:
                fields = line.split(",")
                if len(fields) < 3:
                    raise Exception("Expected >=3 fields per line in %s" % path)

                file = str(Path(reldir, fields[0]))

                csv.entries.append(ShimBootEntry(file, fields[1], fields[2]))
        return csv
