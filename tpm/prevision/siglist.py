# SPDX-License-Identifier: LGPL-2.1-or-later

from uuid import UUID

def empty_cert_list(type_guid, owner_guid):
    null = bytes([0] * 32)
    return (type_guid.bytes_le +
            int(76).to_bytes(4, 'little') +
            int(0).to_bytes(4, 'little') +
            int(48).to_bytes(4, 'little') +
            owner_guid.bytes_le +
            null)

def empty_cert_list_for_shim():
    typ = UUID("c1c41626-504c-4092-aca941f936934328")
    owner = UUID("605dab50-e046-4300-abb63dd810dd8b23")
    return empty_cert_list(typ, owner)
