# SPDX-License-Identifier: LGPL-2.1-or-later

import yaml

class ImageManifest:

    def __init__(self, path):

        self.shim = {
            "enabled": False,
            "path": "/boot/efi/EFI/BOOT/BOOTX64.EFI",
            "features": {
                "mok-list-trusted": True,
            },
            "vendor": {
                "db": "shim-vendor.esl",
            },
            "cert": {
                "guid": None,
                "path": "db-ms.cer",
            },
        }
        self.grub = {
            "enabled": False,
            "path": None,
            "config": "/boot/grub2/grub.cfg",
            "cert": {
                "guid": None,
                "path": "db-rh.cer",
            },
        }
        self.systemd_boot = {
            "enabled": False,
            "path": "/boot/systemd/systemd-bootx64.efi",
            "cert": {
                "guid": None,
                "path": "db-rh.cer",
            },
        }
        self.systemd_stub = {
            "enabled": True,
            "features": {
                "three-pcrs": True,
            },
        }
        self.systemd_pcrphase = {
            "enabled": False,
        }
        self.linux = {
            "path": None,
            "features": {
                "measure-initrd": True
            },
            "cert": {
                "guid": None,
                "path": "db-rh-uki.cer",
            },
        }

        self.load(path)

    def load(self, path):
        with open(path, "rb") as fh:
            cfg = yaml.safe_load(fh)

        self.shim["enabled"] = cfg.get("shim", {}).get("enabled", self.shim["enabled"])
        if self.shim["enabled"]:
            self.shim["path"] = cfg.get("shim", {}).get("path", self.shim["path"])
            for k in self.shim["features"].keys():
                self.shim["features"][k] = cfg.get("shim", {}).get("features", {}).get(k, self.shim["features"][k])
            for k in self.shim["vendor"].keys():
                self.shim["vendor"][k] = cfg.get("shim", {}).get("vendor", {}).get(k, self.shim["vendor"][k])
            for k in self.shim["cert"].keys():
                self.shim["cert"][k] = cfg.get("shim", {}).get("cert", {}).get(k, self.shim["cert"][k])

        self.grub["enabled"] = cfg.get("grub", {}).get("enabled", self.grub["enabled"])
        if self.grub["enabled"]:
            self.grub["path"] = cfg.get("grub", {}).get("path", self.grub["path"])
            self.grub["config"] = cfg.get("grub", {}).get("path", self.grub["config"])
            for k in self.grub["cert"].keys():
                self.grub["cert"][k] = cfg.get("grub", {}).get("cert", {}).get(k, self.grub["cert"][k])

        self.systemd_boot["enabled"] = cfg.get("systemd-boot", {}).get("enabled", self.systemd_boot["enabled"])
        if self.systemd_boot["enabled"]:
            self.systemd_boot["path"] = cfg.get("systemd-boot", {}).get("path", self.systemd_boot["path"])
            for k in self.systemd_boot["cert"].keys():
                self.systemd_boot["cert"][k] = cfg.get("systemd-boot", {}).get("cert", {}).get(k, self.systemd_boot["cert"][k])

        self.systemd_stub["enabled"] = cfg.get("systemd-stub", {}).get("enabled", self.systemd_stub["enabled"])
        if self.systemd_stub["enabled"]:
            for k in self.systemd_stub["features"].keys():
                self.systemd_stub["features"][k] = cfg.get("systemd-stub", {}).get("features", {}).get(k, self.systemd_stub["features"][k])

        self.systemd_pcrphase["enabled"] = cfg.get("systemd-pcrphase", {}).get("enabled", self.systemd_pcrphase["enabled"])

        self.linux["path"] = cfg.get("linux", {}).get("path", self.linux["path"])
        for k in self.linux["cert"].keys():
            self.linux["cert"][k] = cfg.get("linux", {}).get("cert", {}).get(k, self.linux["cert"][k])
        for k in self.linux["features"].keys():
            self.linux["features"][k] = cfg.get("linux", {}).get("features", {}).get(k, self.linux["features"][k])

class FirmwareManifest:

    def __init__(self, path):
        self.secureboot = {
            "enabled": True,
            "pk": None,
            "kek": None,
            "db": None,
            "dbx": None,
        }
        self.type = "edk2"
        self.digests = []
        self.optionroms = []
        self.vm_id = None

        self.load(path)

    def load(self, path):
        with open(path, "rb") as fh:
            cfg = yaml.safe_load(fh)

        self.secureboot["enabled"] = cfg.get("secureboot", {}).get("enabled", self.secureboot["enabled"])
        self.secureboot["pk"] = cfg.get("secureboot", {}).get("pk", self.secureboot["pk"])
        self.secureboot["kek"] = cfg.get("secureboot", {}).get("kek", self.secureboot["kek"])
        self.secureboot["db"] = cfg.get("secureboot", {}).get("db", self.secureboot["db"])
        self.secureboot["dbx"] = cfg.get("secureboot", {}).get("dbx", self.secureboot["dbx"])

        self.type = cfg.get("type", self.type)
        self.digests = cfg.get("digests", self.digests)
        self.optionroms = cfg.get("option-roms", self.optionroms)
        self.vm_id = cfg.get("vm", {}).get("id", self.vm_id)
