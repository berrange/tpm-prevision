# SPDX-License-Identifier: LGPL-2.1-or-later

import abc
from uuid import UUID

# https://uefi.org/sites/default/files/resources/UEFI_Spec_2_8_final.pdf
#  Section 3.1.3 Load Options

class EFIBootEntry:

    ATTR_ACTIVE = 0x1
    ATTR_FORCE_RECONNECT = 0x2
    ATTR_HIDDEN = 0x8
    ATTR_CATEGORY = 0x1f00

    ATTR_CATEGORY_BOOT = 0x0
    ATTR_CATEGORY_APP = 0x100

    def __init__(self, attr, label, paths, data=None):
        self.attr = attr
        self.label = label
        self.paths = paths
        self.data = data

    def __repr__(self):
        return "<boot-entry:attr=%x,label=%s,paths=%s,data=%s>" % (
            self.attr, self.label, self.paths, self.data
            )

    def encode(self):
        data = bytes([])
        data += self.attr.to_bytes(4, 'little')
        pdata = bytes([])
        for path in self.paths:
            pdata += path.encode()
        data += len(pdata).to_bytes(2, 'little')
        data += (self.label + "\0").encode('utf-16-le')
        data += pdata
        if self.data is not None:
            data += self.data
        return data


# https://uefi.org/sites/default/files/resources/UEFI_Spec_2_8_final.pdf
#  Section 10.3 Device Node Paths

class EFIPath(abc.ABC):

    PATH_TYPE_HARDWARE = 1
    PATH_TYPE_ACPI = 2
    PATH_TYPE_MESSAGING = 3
    PATH_TYPE_MEDIA = 4
    PATH_TYPE_BIOS = 5
    PATH_TYPE_TERMINATOR = 0x7f

    def __init__(self, pathtype, pathsubtype):
        self.pathtype = pathtype
        self.pathsubtype = pathsubtype

    def encode(self):
        return (self.pathtype.to_bytes(1) +
                self.pathsubtype.to_bytes(1))

    @classmethod
    def decode_path(cls, data):
        components = []
        while len(data) != 0:
            pathtype = int.from_bytes(data[0:1])
            pathsubtype = int.from_bytes(data[1:2])

            subcls = None
            if pathtype == cls.PATH_TYPE_HARDWARE:
                subcls = EFIHardwarePath.class_for_subtype(pathsubtype)
            elif pathtype == cls.PATH_TYPE_ACPI:
                subcls = EFIACPIPath.class_for_subtype(pathsubtype)
            elif pathtype == cls.PATH_TYPE_MESSAGING:
                raise Exception("Messaging path unsupported")
            elif pathtype == cls.PATH_TYPE_MEDIA:
                subcls = EFIMediaPath.class_for_subtype(pathsubtype)
            elif pathtype == cls.PATH_TYPE_BIOS:
                raise Exception("BIOS path unsupported")
            elif pathtype == cls.PATH_TYPE_TERMINATOR:
                subcls = EFIPathTerminator.class_for_subtype(pathsubtype)
            else:
                raise Exception("Unknown path type %d" % pathtype)

            path, data = subcls.decode(data[2:])
            components.append(path)
        return components

class EFIPathTerminator(EFIPath):
    def __init__(self):
        super().__init__(EFIPath.PATH_TYPE_TERMINATOR, 0xff)

    def encode(self):
        return (super().encode() +
                int(4).to_bytes(2, 'little'))

    def __repr__(self):
        return "[terminator]"

    @classmethod
    def class_for_subtype(cls, pathsubtype):
        if pathsubtype == 0xff:
            return cls
        else:
            raise Exception("Unknown terminator path subtype %d" % pathsubtype)

    @classmethod
    def decode(cls, data):
        assert(int.from_bytes(data[0:2], 'little') == 4)
        assert(len(data) == 2)
        return cls(), bytes([])

class EFIACPIPath(EFIPath):

    ACPI_TYPE_ACPI = 1
    ACPI_TYPE_ACPI_EXTENDED = 2
    ACPI_TYPE_ADR = 3
    ACPI_TYPE_NVDIMM = 4

    def __init__(self, pathsubtype):
        super().__init__(EFIPath.PATH_TYPE_ACPI, pathsubtype)

    @classmethod
    def class_for_subtype(cls, pathsubtype):
        if pathsubtype == cls.ACPI_TYPE_ACPI:
            return EFIACPIPathACPI
        elif pathsubtype == cls.ACPI_TYPE_ACPI_EXTENDED:
            raise Exception("ACPI Extended")
        elif pathsubtype == cls.ACPI_TYPE_ADR:
            raise Exception("ACPI ADR")
        elif pathsubtype == cls.ACPI_TYPE_NVDIMM:
            raise Exception("ACPI NVDIMM")
        else:
            raise Exception("Unknown ACPI subtype %d" % pathsubtype)

class EFIACPIPathACPI(EFIACPIPath):

    def __init__(self, hid, uid):
        super().__init__(EFIACPIPath.ACPI_TYPE_ACPI)
        self.hid = hid
        self.uid = uid

    def __repr__(self):
        return "[acpi:acpi:hid=0x%x,uid=0x%x]" % (self.hid, self.uid)

    def encode(self):
        return (super().encode() +
                int(12).to_bytes(2, 'little') +
                self.hid.to_bytes(4, 'little') +
                self.uid.to_bytes(4, 'little'))

    @classmethod
    def decode(cls, data):
        assert(int.from_bytes(data[0:2], 'little') == 12)
        return cls(int.from_bytes(data[2:6], 'little'),
                   int.from_bytes(data[6:10], 'little')), data[10:]

class EFIHardwarePath(EFIPath):

    HARDWARE_TYPE_PCI = 1
    HARDWARE_TYPE_PCCARD = 2
    HARDWARE_TYPE_MMAPED = 3
    HARDWARE_TYPE_VENDOR = 4
    HARDWARE_TYPE_CONTROLLER = 5
    HARDWARE_TYPE_BMC = 6

    def __init__(self, pathsubtype):
        super().__init__(EFIPath.PATH_TYPE_HARDWARE, pathsubtype)

    @classmethod
    def class_for_subtype(cls, pathsubtype):
        if pathsubtype == cls.HARDWARE_TYPE_PCI:
            return EFIHardwarePathPCI
        elif pathsubtype == cls.HARDWARE_TYPE_PCCARD:
            raise Exception("Hardware PCCard")
        elif pathsubtype == cls.HARDWARE_TYPE_MMAPED:
            raise Exception("Hardware mmaped")
        elif pathsubtype == cls.HARDWARE_TYPE_VENDOR:
            raise Exception("Hardware vendor")
        elif pathsubtype == cls.HARDWARE_TYPE_CONTROLLER:
            raise Exception("Hardware controller")
        elif pathsubtype == cls.HARDWARE_TYPE_BMC:
            raise Exception("Hardware BMC")
        else:
            raise Exception("Unknown hardware subtype %d" % pathsubtype)

class EFIHardwarePathPCI(EFIHardwarePath):

    def __init__(self, function, device):
        super().__init__(EFIHardwarePath.HARDWARE_TYPE_PCI)
        self.function = function
        self.device = device

    def __repr__(self):
        return "[hardware:pci:function=0x%x,device=0x%x]" % (
            self.function, self.device)

    def encode(self):
        return (super().encode() +
                int(6).to_bytes(2, 'little') +
                self.function.to_bytes(1) +
                self.device.to_bytes(1))

    @classmethod
    def decode(cls, data):
        assert(int.from_bytes(data[0:2], 'little') == 6)
        return cls(int.from_bytes(data[2:3], 'little'),
                   int.from_bytes(data[3:4], 'little')), data[4:]


class EFIMediaPath(EFIPath):

    MEDIA_TYPE_HARD_DRIVE = 1
    MEDIA_TYPE_CDROM = 2
    MEDIA_TYPE_VENDOR = 3
    MEDIA_TYPE_FILE = 4
    MEDIA_TYPE_MEDIA_PROTOCOL = 5
    MEDIA_TYPE_PIWG_FIRMWARE_FILE = 6
    MEDIA_TYPE_PIWG_FIRMWARE_VOLUME = 7
    MEDIA_TYPE_REATIVE_OFFSET_RANGE = 8
    MEDIA_TYPE_RAM_DISK = 9

    def __init__(self, pathsubtype):
        super().__init__(EFIPath.PATH_TYPE_MEDIA, pathsubtype)

    @classmethod
    def class_for_subtype(cls, pathsubtype):
        if pathsubtype == cls.MEDIA_TYPE_HARD_DRIVE:
            return EFIMediaPathHardDrive
        elif pathsubtype == cls.MEDIA_TYPE_CDROM:
            raise Exception("Media CDROM")
        elif pathsubtype == cls.MEDIA_TYPE_VENDOR:
            return EFIMediaPathVendor
        elif pathsubtype == cls.MEDIA_TYPE_FILE:
            return EFIMediaPathFile
        elif pathsubtype == cls.MEDIA_TYPE_MEDIA_PROTOCOL:
            raise Exception("Media media protocol")
        elif pathsubtype == cls.MEDIA_TYPE_PIWG_FIRMWARE_FILE:
            return EFIMediaPathPIWGFirmwareFile
        elif pathsubtype == cls.MEDIA_TYPE_PIWG_FIRMWARE_VOLUME:
            return EFIMediaPathPIWGFirmwareVolume
        elif pathsubtype == cls.MEDIA_TYPE_REATIVE_OFFSET_RANGE:
            raise Exception("Media relative offset range")
        elif pathsubtype == cls.MEDIA_TYPE_RAM_DISK:
            raise Exception("Media RAM disk")
        else:
            raise Exception("Unknown media subtype %d" % pathsubtype)

class EFIMediaPathHardDrive(EFIMediaPath):

    FMT_MBR = 1
    FMT_GPT = 2

    SIG_TYPE_NONE = 0
    SIG_TYPE_MBR = 1
    SIG_TYPE_GUID = 2

    def __init__(self, partnum, lbaStart, lbaSize, signature, fmt, sigType):
        super().__init__(EFIMediaPath.MEDIA_TYPE_HARD_DRIVE)
        self.partnum = partnum
        self.lbaStart = lbaStart
        self.lbaSize = lbaSize
        self.signature = signature
        self.fmt = fmt
        self.sigType = sigType

    def __repr__(self):
        return "[hardware:harddrive:partnum=%d,lbaStart=0x%x,lbaSize=0x%x,sig=%s,fmt=0x%x,sigType=0x%x]" % (
            self.partnum, self.lbaStart, self.lbaSize, self.signature, self.fmt, self.sigType)

    def encode(self):
        return (super().encode() +
                int(42).to_bytes(2, 'little') +
                self.partnum.to_bytes(4, 'little') +
                self.lbaStart.to_bytes(8, 'little') +
                self.lbaSize.to_bytes(8, 'little') +
                self.signature.bytes_le +
                self.fmt.to_bytes(1) +
                self.sigType.to_bytes(1))

    @classmethod
    def decode(cls, data):
        assert(int.from_bytes(data[0:2], 'little') == 42)
        return cls(int.from_bytes(data[2:6], 'little'),
                   int.from_bytes(data[6:14], 'little'),
                   int.from_bytes(data[14:22], 'little'),
                   UUID(bytes=data[22:38]),
                   int.from_bytes(data[38:39]),
                   int.from_bytes(data[39:40])), data[40:]

class EFIMediaPathVendor(EFIMediaPath):

    def __init__(self, guid, data):
        super().__init__(EFIMediaPath.MEDIA_TYPE_VENDOR)
        self.guid = guid
        self.data = data

    def __repr__(self):
        return "[hardware:vendor:guid=%s,data=%s]" % (
            self.guid, self.data.hex())

    def encode(self):
        length = 4 + 16 + len(self.data)
        return (super().encode() +
                length.to_bytes(2, 'little') +
                self.guid.bytes_le +
                data)

    @classmethod
    def decode(cls, data):
        length = int.from_bytes(data[0:2], 'little')  - 2
        return cls(UUID(bytes=data[2:18]),
                   data[18:length]), data[length:]


class EFIMediaPathFile(EFIMediaPath):

    def __init__(self, path):
        super().__init__(EFIMediaPath.MEDIA_TYPE_FILE)
        self.path = path

    def __repr__(self):
        return "[media:file:path=%s]" % (
            self.path)

    def encode(self):
        length = 4 + ((len(self.path) + 1)*2)
        return (super().encode() +
                length.to_bytes(2, 'little') +
                (self.path + "\0").encode('utf-16-le'))

    @classmethod
    def decode(cls, data):
        length = int.from_bytes(data[0:2], 'little') - 2
        assert(length % 2 == 0)
        return cls(data[2:length].decode('utf-16-le')), data[length:]


class EFIMediaPathPIWGFirmwareFile(EFIMediaPath):

    def __init__(self, guid):
        super().__init__(EFIMediaPath.MEDIA_TYPE_PIWG_FIRMWARE_FILE)
        self.guid = guid

    def __repr__(self):
        return "[media:piwg-fw-file:guid=%s]" % (
            self.guid)

    def encode(self):
        length = 20
        return (super().encode() +
                length.to_bytes(2, 'little') +
                self.guid.bytes_le)

    @classmethod
    def decode(cls, data):
        assert(int.from_bytes(data[0:2], 'little') == 20)
        return cls(UUID(bytes=data[2:])), data[18:]


class EFIMediaPathPIWGFirmwareVolume(EFIMediaPath):

    def __init__(self, guid):
        super().__init__(EFIMediaPath.MEDIA_TYPE_PIWG_FIRMWARE_VOLUME)
        self.guid = guid

    def __repr__(self):
        return "[media:piwg-fw-volume:guid=%s]" % (
            self.guid)

    def encode(self):
        length = 20
        return (super().encode() +
                length.to_bytes(2, 'little') +
                self.guid.bytes_le)

    @classmethod
    def decode(cls, data):
        assert(int.from_bytes(data[0:2], 'little') == 20)
        return cls(UUID(bytes=data[2:])), data[18:]
