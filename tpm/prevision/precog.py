# SPDX-License-Identifier: LGPL-2.1-or-later

from pathlib import Path

from .tpm import TPM
from .eventlog import *
from .eventdata import *
from .efiboot import *
from . import siglist
from .shim import ShimSBAT, ShimBootCSV

class Precognitive:

    def __init__(self, firmware_manifest, image_file, image_mount, image_manifest):
        self.firmware_manifest = firmware_manifest
        self.image_file = image_file
        self.image_mount = image_mount
        self.image_manifest = image_manifest

        self.seen_shim_certs = {}

    def _get_image_file_path(self, path):
        return Path(self.image_mount, path[1:])

    def _get_firmware_next(self):
        # Normally shim will be first thing
        if self.image_manifest.shim["enabled"]:
            return self.image_manifest.shim["path"], self.image_manifest.shim["cert"]

        # If user signs their own binaries, the bootloader
        # might be the first thing
        if self.image_manifest.grub["enabled"]:
            return self.image_manifest.grub["path"], self.image_manifest.grub["cert"]
        elif self.image_manifest.systemd_boot["enabled"]:
            return self.image_manifest.systemd_boot["path"], self.image_manifest.systemd_boot["cert"]

        # Its possible to boot linux directly
        return self.image_manifest.linux["path"], self.image_manifest.linux["cert"]

    def _vision_firmware_preboot(self, tpm):
        if self.firmware_manifest.type not in ["azure", "ovmf"]:
            raise Exception("Unsupported firmware type: %s" % self.firmware_manifest.type)

        tpm.extend(EventSCRTM(pcr=0, version=0))

        for d in self.firmware_manifest.digests:
            tpm.extend(EventEFIPlatformFirmwareBlob(
                pcr=0, data=EventDataPreHashed(d)))


        # EDK2
        if self.firmware_manifest.secureboot["enabled"]:
            tpm.extend(EventEFIVariableDriverConfig(
                pcr=7,
                data=EventDataVariable(
                    EventDataVariable.GUID_GLOBAL_VARIABLE,
                    "SecureBoot", bytes.fromhex("01"))))

            # EDK2 (PK ESL file)
            tpm.extend(EventEFIVariableDriverConfig(
                pcr=7,
                data=EventDataVariable.from_cert_list_file(
                    EventDataVariable.GUID_GLOBAL_VARIABLE,
                    "PK", self.firmware_manifest.secureboot["pk"])))

            # EDK2 (KEK ESL file)
            tpm.extend(EventEFIVariableDriverConfig(
                pcr=7,
                data=EventDataVariable.from_cert_list_file(
                    EventDataVariable.GUID_GLOBAL_VARIABLE,
                    "KEK", self.firmware_manifest.secureboot["kek"])))

            # EDK2 (DB ESL file)
            tpm.extend(EventEFIVariableDriverConfig(
                pcr=7,
                data=EventDataVariable.from_cert_list_file(
                    EventDataVariable.GUID_SECURITY_DATABASE,
                    "db", self.firmware_manifest.secureboot["db"])))

            # EDK2 (DBX ESL file)
            tpm.extend(EventEFIVariableDriverConfig(
                pcr=7,
                data=EventDataVariable.from_cert_list_file(
                    EventDataVariable.GUID_SECURITY_DATABASE,
                    "dbx", self.firmware_manifest.secureboot["dbx"])))

        else:

            tpm.extend(EventEFIVariableDriverConfig(
                pcr=7,
                data=EventDataVariable(
                    EventDataVariable.GUID_GLOBAL_VARIABLE,
                    "SecureBoot", bytes.fromhex("00"))))

        # EDK2
        tpm.extend(EventSeparator(pcr=7))

        if self.firmware_manifest.type == "azure":
            tpm.extend(EventCompactHash(
                pcr=6,
                data=EventDataAzureVMIdentifier(UUID(self.firmware_manifest.vm_id))))

        # EDK2
        for rom in self.firmware_manifest.optionroms:
            tpm.extend(EventEFIBootServicesDriver(
                pcr=2,
                data=EventDataOptionROMAuthenticode(rom)))

        gpt = GPTHeader.from_device(self.image_file)

        bootindexes = []
        bootentries = []
        if self.firmware_manifest.type == "ovmf":
            bootindexes.prepend(len(bootentries))
            bootentries.prepend(EventDataBootEntry(EFIBootEntry(
                EFIBootEntry.ATTR_ACTIVE |
                EFIBootEntry.ATTR_HIDDEN |
                EFIBootEntry.ATTR_CATEGORY_APP,
                "UiApp",
                [EFIMediaPathPIWGFirmwareVolume(UUID("7cb8bdc9-f8eb-4f34-aaea-3ee4af6516a1")),
                 EFIMediaPathPIWGFirmwareFile(UUID("462caa21-7614-4503-836e-8ab6f4662331")),
                 EFIPathTerminator()])))

            if self.image_manifest.shim.enabled:
                bootindexes.prepend(len(bootentries))
                bootentries.prepend(EventDataBootEntry(EFIBootEntry(
                    EFIBootEntry.ATTR_ACTIVE,
                    "UEFI Misc Device",
                    [EFIACPIPathACPI(0x0a0341d0, 0x00000000),
                     EFIHardwarePathPCI(0x3, 0x2),
                     EFIHardwarePathPCI(0x0, 0x0),
                     EFIPathTerminator()],
                    bytes.fromhex("4eac0881119f594d850ee21a522c59b2"))))

        if self.image_manifest.shim["enabled"]:
            shimdir = Path(self.image_manifest.shim["path"]).parent
            bootcsvfile = Path(shimdir, "BOOTX64.CSV")
            bootcsv = ShimBootCSV.from_file(
                self._get_image_file_path(str(bootcsvfile)))
            userbootentries = []
            userbootindexes = []
            for entry in bootcsv.entries:
                userbootindexes.append(len(bootentries)+len(userbootentries))
                userbootentries.append(EFIBootEntry(
                    EFIBootEntry.ATTR_ACTIVE,
                    entry.label,
                    entry.efi_path(gpt),
                    entry.data.encode("utf-16-le")))

            bootindexes = userbootindexes + bootindexes
            bootentries = userbootentries + bootentries
        # XXXX how to populate boot entries if shim isn't used ?

        # EDK2
        if self.firmware_manifest.type == "azure":
            bootindexes = [b + 1 for b in bootindexes]
        tpm.extend(EventEFIVariableBoot(
            pcr=1,
            data=EventDataBootOrder(bootindexes)))

        # EDK2 (Boot000n)
        for entry in bootentries:
            tpm.extend(EventEFIVariableBoot(
                pcr=1,
                data=EventDataBootEntry(entry)))

        # EDK2
        tpm.extend(EventEFIAction(
            pcr=4,
            strval="Calling EFI Application from Boot Option"))

        # EDK2
        tpm.extend(EventSeparator(pcr=0))
        # EDK2
        tpm.extend(EventSeparator(pcr=1))
        # EDK2
        tpm.extend(EventSeparator(pcr=2))
        # EDK2
        tpm.extend(EventSeparator(pcr=3))
        # EDK2
        tpm.extend(EventSeparator(pcr=4))
        # EDK2
        tpm.extend(EventSeparator(pcr=5))
        # EDK2
        tpm.extend(EventSeparator(pcr=6))

        # EDK2 (DB entry that validated first booted binary)
        path, cert = self._get_firmware_next()
        tpm.extend(EventEFIVariableAuthority(
            pcr=7,
            data=EventDataVariable.from_guid_cert_file(
                EventDataVariable.GUID_SECURITY_DATABASE,
                "db",
                UUID(cert["guid"]),
                cert["path"])))

        # EDK2 (Root partition table)
        tpm.extend(EventEFIGPTEvent(
            pcr=5, gpt=gpt))

        # EDK2 (First bootloader app, shim)
        tpm.extend(EventEFIBootServicesApplication(
            pcr=4,
            data=EventDataPEImageAuthenticode(
                self._get_image_file_path(path))))

    def _vision_firmware_postboot(self, tpm):
        if self.firmware_manifest.type not in ["azure", "ovmf"]:
            raise Exception("Unsupported firmware type: %s" % self.firmware_manifest.type)

        # EDK2
        tpm.extend(EventEFIAction(
            pcr=5,
            strval="Exit Boot Services Invocation"))
        # EDK2
        tpm.extend(EventEFIAction(
            pcr=5,
            strval="Exit Boot Services Returned with Success"))

    def _log_shim_db_cert(self, tpm, cert):
        if cert["path"] in self.seen_shim_certs:
            return

        self.seen_shim_certs[cert["path"]] = True
        if cert["guid"] is None:
            tpm.extend(EventEFIVariableAuthority(
                pcr=7,
                data=EventDataVariable.from_cert_file(
                    EventDataVariable.GUID_SHIM_LOCK,
                    "Shim",
                    cert["path"])))
        else:
            tpm.extend(EventEFIVariableAuthority(
                pcr=7,
                data=EventDataVariable.from_guid_cert_file(
                    EventDataVariable.GUID_SECURITY_DATABASE,
                    "vendor_db",
                    UUID(cert["guid"]),
                    cert["path"])))

    def _get_shim_next(self):
        # The bootloader is usually the first thing
        if self.image_manifest.grub["enabled"]:
            return self.image_manifest.grub["path"], self.image_manifest.grub["cert"]
        elif self.image_manifest.systemd_boot["enabled"]:
            return self.image_manifest.systemd_boot["path"],  self.image_manifest.systemd_boot["cert"]

        # Its possible to boot linux directly
        return self.image_manifest.linux["path"], self.image_manifest.linux["cert"]

    def _vision_shim(self, tpm):
        # shim (MokList in ESL format)
        tpm.extend(EventIPL(
            pcr=14,
            data=EventDataRaw.from_file(self.image_manifest.shim["vendor"]["db"])))

        # shim (MokListX in ESL format)
        tpm.extend(EventIPL(
            pcr=14,
            data=EventDataRaw(siglist.empty_cert_list_for_shim())))

        # shim (Sbat CSV contents)
        # XXX configurable sbat
        tpm.extend(EventEFIVariableAuthority(
            pcr=7,
            data=EventDataVariable(
                EventDataVariable.GUID_SHIM_LOCK,
                "SbatLevel",
                ShimSBAT().add_default_component().encode())))

        if self.image_manifest.shim["features"]["mok-list-trusted"]:
            # shim (MokListTrusted flag)
            tpm.extend(EventEFIVariableAuthority(
                pcr=7,
                data=EventDataVariable(
                    EventDataVariable.GUID_SHIM_LOCK,
                    "MokListTrusted", bytes.fromhex("01"))))

            # shim (MokListTrusted flag)
            tpm.extend(EventIPL(
                pcr=14,
                data=EventDataHex("01")))

        # shim (DB entry that validated next binary)
        path, cert = self._get_shim_next()
        self._log_shim_db_cert(tpm, cert)

        # shim (next binary)
        tpm.extend(EventEFIBootServicesApplication(
            pcr=4,
            data=EventDataPEImageAuthenticode(
                self._get_image_file_path(path))))

    def _vision_systemd_boot(self, tpm):
        # shim
        tpm.extend(EventEFIBootServicesApplication(
            pcr=4,
            data=EventDataPEImageAuthenticode(
                self._get_image_file_path(
                    self.image_manifest.linux["path"]))))

        # shim (DB entry that validated linux, if different fromprevious vendor_db)
        cert = self.image_manifest.linux["cert"]
        self._log_shim_db_cert(tpm, cert)

    def _vision_grub(self, tpm):
        raise Exception("Grub precognition not implemented")

    def _vision_systemd_stub(self, tpm, peimage):
        if not self.image_manifest.systemd_stub["features"]["three-pcrs"]:
            return

        # systemd-stub (cmdline to be booted)
        tpm.extend(EventIPL(
            pcr=12,
            data=EventDataPEImageSection(peimage, ".cmdline",
                                         strencode="utf-16-le")))

        # systemd-stub (UKI section name)
        tpm.extend(EventIPL(
            pcr=11,
            data=EventDataString(".linux\0", "ascii")))
        # systemd-stub (UKI section content)
        tpm.extend(EventIPL(
            pcr=11,
            data=EventDataPEImageSection(peimage, ".linux")))

        if peimage.has_section(".osrel"):
            # systemd-stub (UKI section name)
            tpm.extend(EventIPL(
                pcr=11,
                data=EventDataString(".osrel\0", "ascii")))
            # systemd-stub (UKI section content)
            tpm.extend(EventIPL(
                pcr=11,
                data=EventDataPEImageSection(peimage, ".osrel")))

        # systemd-stub (UKI section name)
        tpm.extend(EventIPL(
            pcr=11,
            data=EventDataString(".cmdline\0", "ascii")))
        # systemd-stub (UKI section content)
        tpm.extend(EventIPL(
            pcr=11,
            data=EventDataPEImageSection(peimage, ".cmdline")))

        # systemd-stub (UKI section name)
        tpm.extend(EventIPL(
            pcr=11,
            data=EventDataString(".initrd\0", "ascii")))
        # systemd-stub (UKI section content)
        tpm.extend(EventIPL(
            pcr=11,
            data=EventDataPEImageSection(peimage, ".initrd")))

    def _vision_linux(self, tpm):
        peimage = PEImage(self._get_image_file_path(
            self.image_manifest.linux["path"]))

        if self.image_manifest.systemd_stub["enabled"]:
            self._vision_systemd_stub(tpm, peimage)

        # XXX figure out exactly what causes measurement of
        # the inner vmlinux, as it doesn't always happen
        if False:
            linux = peimage.get_section_data(".linux")
            tpm.extend(EventEFIBootServicesApplication(
                pcr=4,
                data=EventDataPEImageAuthenticode(data=linux)))

        if self.image_manifest.linux["features"]["measure-initrd"]:
            # Linux (cmdline received)
            tpm.extend(EventEventTag(
                pcr=9,
                data=EventDataPEImageSection(peimage, ".cmdline",
                                             strencode="utf-16-le")))

            # Linux (initrd received)
            tpm.extend(EventEventTag(
                pcr=9,
                data=EventDataPEImageSection(peimage, ".initrd")))

    def _vision_systemd_pcrphase(self, tpm):
        tpm.extend(EventIPL(
            pcr=11,
            data=EventDataString("sysinit", "ascii")))

        tpm.extend(EventIPL(
            pcr=11,
            data=EventDataString("ready", "ascii")))

    def vision(self):
        tpm = TPM()

        if self.firmware_manifest.type == "ovmf":
            for p in range(17, 23):
                tpm.initialize(p, bytes([255] * 32))

        self._vision_firmware_preboot(tpm)

        if self.image_manifest.shim["enabled"]:
            self._vision_shim(tpm)
        if self.image_manifest.grub["enabled"]:
            self._vision_grub(tpm)
        if self.image_manifest.systemd_boot["enabled"]:
            self._vision_systemd_boot(tpm)

        self._vision_linux(tpm)

        self._vision_firmware_postboot(tpm)

        if self.image_manifest.systemd_pcrphase["enabled"]:
            self._vision_systemd_pcrphase(tpm)

        return tpm



