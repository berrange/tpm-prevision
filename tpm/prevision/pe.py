# SPDX-License-Identifier: LGPL-2.1-or-later

import hashlib
from io import BytesIO

class PEImage:

    # https://learn.microsoft.com/en-us/windows/win32/debug/pe-format
    # https://upload.wikimedia.org/wikipedia/commons/1/1b/Portable_Executable_32_bit_Structure_in_SVG_fixed.svg

    # Magic bytes 'MZ'
    DOS_SIG = bytes([0x4d, 0x5a])

    # Offset in DOS header where the PE offset live
    DOS_HEADER_FIELD_PE_OFFSET = 0x3c

    # Magic bytes 'PE\0\0'
    PE_SIG = bytes([0x50, 0x45, 0x00, 0x00])

    # Offsets relative to start of PE_SIG
    PE_HEADER_FIELD_MACHINE = 0x4
    PE_HEADER_FIELD_NUMBER_OF_SECTIONS = 0x6
    PE_HEADER_FIELD_SIZE_OF_OPTIONAL_HEADER = 0x14
    PE_HEADER_SIZE = 0x18

    MACHINE_I686 = 0x014c
    MACHINE_X86_64 = 0x8664
    MACHINE_AARCH64 = 0xaa64

    MACHINES = [MACHINE_I686,
                MACHINE_X86_64,
                MACHINE_AARCH64]


    PE32_SIG = bytes([0x0b, 0x01])
    PE32P_SIG = bytes([0x0b, 0x02])

    # Offsets relative to PE_HEADER_SIZE
    PE32X_HEADER_FIELD_SIZE_OF_HEADERS = 0x3c
    PE32X_HEADER_FIELD_CHECKSUM = 0x40
    PE32_HEADER_FIELD_CERT_TABLE = 0x80
    PE32P_HEADER_FIELD_CERT_TABLE = 0x90

    SECTION_HEADER_FIELD_VIRTUAL_SIZE = 0x8
    SECTION_HEADER_FIELD_SIZE = 0x10
    SECTION_HEADER_FIELD_OFFSET = 0x14
    SECTION_HEADER_LENGTH = 0x28

    def __init__(self, path=None, data=None):
        self.path = path
        self.data = data
        self.sections = {}
        self.authenticodehash = self.load()

    def has_section(self, name):
        return name in self.sections

    def get_section_data(self, name):
        if name not in self.sections:
            raise Exception("Missing section '%s'" % name)

        start, end = self.sections[name]
        with open(self.path, 'rb') as fp:
            fp.seek(start)
            return fp.read(end-start)

    def get_authenticode_hash(self):
        return self.authenticodehash

    def load(self):
        if self.path is not None:
            with open(self.path, 'rb') as fp:
                return self.load_fh(fp)
        elif self.data is not None:
            with BytesIO(self.data) as fp:
                return self.load_fh(fp)
        else:
            raise Exception("Either path or data is required")

    def load_fh(self, fp):
        dossig = fp.read(2)
        if dossig != self.DOS_SIG:
            raise Exception("Missing DOS header magic %s got %s" % (self.DOS_SIG, dossig))

        fp.seek(self.DOS_HEADER_FIELD_PE_OFFSET)
        peoffset = int.from_bytes(fp.read(4), 'little')
        fp.seek(peoffset)

        pesig = fp.read(4)
        if pesig != self.PE_SIG:
            raise Exception("Missing PE header magic %s at %x got %s" % (
                self.PE_SIG, peoffset, pesig))

        fp.seek(peoffset + self.PE_HEADER_FIELD_MACHINE)
        pemachine = int.from_bytes(fp.read(2), 'little')

        if pemachine not in self.MACHINES:
            raise Exception("Unexpected PE machine architecture %x expected [%s]" %
                            (pemachine, ", ".join(["%x" % m for m in self.MACHINES])))

        fp.seek(peoffset + self.PE_HEADER_FIELD_NUMBER_OF_SECTIONS)
        numOfSections = int.from_bytes(fp.read(2), 'little')

        fp.seek(peoffset + self.PE_HEADER_FIELD_SIZE_OF_OPTIONAL_HEADER)
        sizeOfOptionalHeader = int.from_bytes(fp.read(2), 'little')

        # image loader header follows PE header
        fp.seek(peoffset + self.PE_HEADER_SIZE)

        ldrsig = fp.read(2)
        if pemachine == self.MACHINE_I686:
            wantldrsig = self.PE32_SIG
            certtableoffset = self.PE32_HEADER_FIELD_CERT_TABLE
        else:
            wantldrsig = self.PE32P_SIG
            certtableoffset = self.PE32P_HEADER_FIELD_CERT_TABLE

        if ldrsig != wantldrsig:
            raise Exception("Missing image loader signature %s got %s" % (
                wantldrsig.hex(), ldrsig.hex()))

        fp.seek(peoffset + self.PE_HEADER_SIZE + self.PE32X_HEADER_FIELD_SIZE_OF_HEADERS)
        sizeOfHeaders = int.from_bytes(fp.read(4), 'little')

        fp.seek(peoffset + self.PE_HEADER_SIZE + certtableoffset + 4)
        certTableSize = int.from_bytes(fp.read(4), 'little')

        # When hashing the file, we need to exclude certain areas
        # with variable data as detailed in:
        #
        # https://reversea.me/index.php/authenticode-i-understanding-windows-authenticode/
        #
        # so build a list of start/end offsets to hash over
        tohash = [
            # From start of file, to the checksum
            [0, peoffset + self.PE_HEADER_SIZE + self.PE32X_HEADER_FIELD_CHECKSUM],

            # From after checksum, to the certificate table
            [peoffset + self.PE_HEADER_SIZE + self.PE32X_HEADER_FIELD_CHECKSUM + 4,
             peoffset + self.PE_HEADER_SIZE + certtableoffset],

            # From after certificate table to end of headers
            [peoffset + self.PE_HEADER_SIZE + certtableoffset + 8,
             sizeOfHeaders],
        ]

        imageDataLength = sizeOfHeaders
        nextSection = peoffset + self.PE_HEADER_SIZE + sizeOfOptionalHeader
        for i in range(numOfSections):
            fp.seek(nextSection)
            name = fp.read(8).decode('ascii').rstrip('\0')

            fp.seek(nextSection + self.SECTION_HEADER_FIELD_OFFSET)
            sectionOffset = int.from_bytes(fp.read(4), 'little')
            fp.seek(nextSection + self.SECTION_HEADER_FIELD_SIZE)
            sectionSize = int.from_bytes(fp.read(4), 'little')
            fp.seek(nextSection + self.SECTION_HEADER_FIELD_VIRTUAL_SIZE)
            sectionVirtualSize = int.from_bytes(fp.read(4), 'little')

            if sectionSize != 0:
                tohash.append([sectionOffset, sectionOffset + sectionSize])
                imageDataLength += sectionSize
                self.sections[name] = [sectionOffset, sectionOffset + sectionVirtualSize]
            nextSection += self.SECTION_HEADER_LENGTH


        fileLength = fp.seek(0, 2) - certTableSize

        if imageDataLength < fileLength:
            tohash.append([imageDataLength, fileLength])

        tohash = sorted(tohash, key=lambda r: r[0])

        h = hashlib.new('sha256')
        for area in tohash:
            fp.seek(area[0])
            h.update(fp.read(area[1]-area[0]))
        return h.digest()
