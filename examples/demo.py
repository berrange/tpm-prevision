#!/usr/bin/python
#
# SPDX-License-Identifier: LGPL-2.1-or-later

from tpm.prevision.tpm import TPM
from tpm.prevision.eventlog import *
from tpm.prevision.eventdata import *
from tpm.prevision.efiboot import *
from tpm.prevision.shim import *
from tpm.prevision import siglist

# Goal is to have a prevision of the PCR values that will
# be reported, as seen in demo.json (from tpm2_eventlog output)

tpm = TPM()
        
# EDK2
tpm.extend(EventSCRTM(pcr=0, version=0))

# EDK2
tpm.extend(EventEFIPlatformFirmwareBlob(
    pcr=0,
    data=EventDataPreHashed("f90144b23b0e4920adc4a429e52dce15bdeb6ed239f4de814fc9154f45785ec4")))
# EDK2
tpm.extend(EventEFIPlatformFirmwareBlob(
    pcr=0,
    data=EventDataPreHashed("39c8074b6d9578fc9d7bde56b46cdf037c6b96e9dbd79b05087b5c8eb6040ac3")))


# EDK2
tpm.extend(EventEFIVariableDriverConfig(
    pcr=7,
    data=EventDataVariable(
        EventDataVariable.GUID_GLOBAL_VARIABLE,
        "SecureBoot", bytes.fromhex("01"))))

# EDK2 (PK ESL file)
tpm.extend(EventEFIVariableDriverConfig(
    pcr=7,
    data=EventDataVariable.from_cert_list(
        EventDataVariable.GUID_GLOBAL_VARIABLE,
        "PK", "var-pk.esl")))

# EDK2 (KEK ESL file)
tpm.extend(EventEFIVariableDriverConfig(
    pcr=7,
    data=EventDataVariable.from_cert_list(
        EventDataVariable.GUID_GLOBAL_VARIABLE,
        "KEK", "var-kek.esl")))

# EDK2 (DB ESL file)
tpm.extend(EventEFIVariableDriverConfig(
    pcr=7,
    data=EventDataVariable.from_cert_list(
        EventDataVariable.GUID_SECURITY_DATABASE,
        "db", "var-db.esl")))

# EDK2 (DBX ESL file)
tpm.extend(EventEFIVariableDriverConfig(
    pcr=7,
    data=EventDataVariable.from_cert_list(
        EventDataVariable.GUID_SECURITY_DATABASE,
        "dbx", "var-dbx.esl")))

# EDK2
tpm.extend(EventSeparator(pcr=7))

# EDK2
tpm.extend(EventEFIBootServicesDriver(
    pcr=2,
    data=EventDataOptionROMAuthenticode("efi-virtio.rom"),))


# EDK2
tpm.extend(EventEFIVariableBoot(
    pcr=1,
    data=EventDataBootOrder([2, 1, 0])))

# EDK2 (Boot0002)
tpm.extend(EventEFIVariableBoot(
    pcr=1,
    data=EventDataBootEntry(EFIBootEntry(
        EFIBootEntry.ATTR_ACTIVE,
        "Shim+SD-Boot",
        [EFIMediaPathHardDrive(1, 0x800, 0x12c000,
                               UUID("423b7f58-3fa3-44d8-8051-e8b767f2896b"),
                               EFIMediaPathHardDrive.FMT_GPT,
                               EFIMediaPathHardDrive.SIG_TYPE_GUID),
         EFIMediaPathFile("\\EFI\\fedora\\shimx64.efi"),
         EFIPathTerminator()],
        "\\systemd-bootx64.efi ".encode('utf-16-le')))))

# EDK2 (Boot0001)
tpm.extend(EventEFIVariableBoot(
    pcr=1,
    data=EventDataBootEntry(EFIBootEntry(
        EFIBootEntry.ATTR_ACTIVE,
        "UEFI Misc Device",
        [EFIACPIPathACPI(0x0a0341d0, 0x00000000),
         EFIHardwarePathPCI(0x3, 0x2),
         EFIHardwarePathPCI(0x0, 0x0),
         EFIPathTerminator()],
        bytes.fromhex("4eac0881119f594d850ee21a522c59b2")))))

# EDK2 (Boot0000)
tpm.extend(EventEFIVariableBoot(
    pcr=1,
    data=EventDataBootEntry(EFIBootEntry(
        EFIBootEntry.ATTR_ACTIVE |
        EFIBootEntry.ATTR_HIDDEN |
        EFIBootEntry.ATTR_CATEGORY_APP,
        "UiApp",
        [EFIMediaPathPIWGFirmwareVolume(UUID("7cb8bdc9-f8eb-4f34-aaea-3ee4af6516a1")),
         EFIMediaPathPIWGFirmwareFile(UUID("462caa21-7614-4503-836e-8ab6f4662331")),
         EFIPathTerminator()]))))

# EDK2
tpm.extend(EventEFIAction(
    pcr=4,
    strval="Calling EFI Application from Boot Option"))

# EDK2
tpm.extend(EventSeparator(pcr=0))
# EDK2
tpm.extend(EventSeparator(pcr=1))
# EDK2
tpm.extend(EventSeparator(pcr=2))
# EDK2
tpm.extend(EventSeparator(pcr=3))
# EDK2
tpm.extend(EventSeparator(pcr=4))
# EDK2
tpm.extend(EventSeparator(pcr=5))
# EDK2
tpm.extend(EventSeparator(pcr=6))

# EDK2 (DB entry that validated shim)
tpm.extend(EventEFIVariableAuthority(
    pcr=7,
    data=EventDataVariable.from_cert_file(
        EventDataVariable.GUID_SECURITY_DATABASE,
        "db", UUID("57cbed38-6675-11ed-8c1c-44a842242ee4"), "var-db.cer")))

# EDK2 (Root partition table)
tpm.extend(EventEFIGPTEvent(
    pcr=5,
    path="/dev/vda"))

# EDK2 (First bootloader app, shim)
tpm.extend(EventEFIBootServicesApplication(
    pcr=4,
    data=EventDataPEImageAuthenticode("/boot/efi/EFI/fedora/shimx64.efi")))

# shim (MokList in ESL format)
tpm.extend(EventIPL(
    pcr=14,
    data=EventDataRaw.from_file("mok-db.esl")))

# shim (MokListX in ESL format)
tpm.extend(EventIPL(
    pcr=14,
    data=EventDataRaw(siglist.empty_cert_list_for_shim())))

# shim (Sbat CSV contents)
tpm.extend(EventEFIVariableAuthority(
    pcr=7,
    data=EventDataVariable(
        EventDataVariable.GUID_SHIM_LOCK,
        "SbatLevel",
        ShimSBAT().add_default_component().encode())))

# shim (MokListTrusted flag)
tpm.extend(EventEFIVariableAuthority(
    pcr=7,
    data=EventDataVariable(
        EventDataVariable.GUID_SHIM_LOCK,
        "MokListTrusted", bytes.fromhex("01"))))

# shim (MokListTrusted flag)
tpm.extend(EventIPL(
    pcr=14,
    data=EventDataHex("01")))

# shim (DB entry that validated sd-boot)
tpm.extend(EventEFIVariableAuthority(
    pcr=7,
    data=EventDataVariable.from_cert_file(
        EventDataVariable.GUID_SECURITY_DATABASE,
        "vendor_db",
        UUID("3b1e2ce2-6a75-11ed-b5e4-44a842242ee4"),
        "var-db-alt.cer")))

# shim (Second boot loader - sd-boot)
tpm.extend(EventEFIBootServicesApplication(
    pcr=4,
    data=EventDataPEImageAuthenticode("/boot/efi/EFI/fedora/systemd-bootx64.efi")))

# shim
tpm.extend(EventEFIBootServicesApplication(
    pcr=4,
    data=EventDataPEImageAuthenticode("/boot/efi/EFI/Linux/linux-6.1.0-0.rc5.39.fc38.x86_64-ed41c2ce518e4f6fada57727f0abe72d.efi")))

# shim (DB entry that validated linux, if different fromprevious vendor_db)
tpm.extend(EventEFIVariableAuthority(
    pcr=7,
    data=EventDataVariable.from_cert_file(
        EventDataVariable.GUID_SECURITY_DATABASE,
        "vendor_db",
        UUID("b6c6f218-6b59-11ed-88d5-44a842242ee4"),
        "var-db-uki.cer")))

# systemd-stub (cmdline to be booted)
tpm.extend(EventIPL(
    pcr=12,
    data=EventDataString(" root=UUID=44965614-4b62-4891-a545-8dcc37d9501a ro rd.luks.uuid=luks-005ab7fb-65ff-4ea6-bef3-655e8c5c635a rhgb quiet \0", "utf-16-le")))

# systemd-stub (UKI section name)
tpm.extend(EventIPL(
    pcr=11,
    data=EventDataString(".linux\0", "ascii")))
# systemd-stub (UKI section content)
tpm.extend(EventIPL(
    pcr=11,
    data=EventDataPEImageSection("/boot/efi/EFI/Linux/linux-6.1.0-0.rc5.39.fc38.x86_64-ed41c2ce518e4f6fada57727f0abe72d.efi", ".linux")))
# systemd-stub (UKI section name)
tpm.extend(EventIPL(
    pcr=11,
    data=EventDataString(".osrel\0", "ascii")))
# systemd-stub (UKI section content)
tpm.extend(EventIPL(
    pcr=11,
    data=EventDataPEImageSection("/boot/efi/EFI/Linux/linux-6.1.0-0.rc5.39.fc38.x86_64-ed41c2ce518e4f6fada57727f0abe72d.efi", ".osrel")))
# systemd-stub (UKI section name)
tpm.extend(EventIPL(
    pcr=11,
    data=EventDataString(".cmdline\0", "ascii")))
# systemd-stub (UKI section content)
tpm.extend(EventIPL(
    pcr=11,
    data=EventDataPEImageSection("/boot/efi/EFI/Linux/linux-6.1.0-0.rc5.39.fc38.x86_64-ed41c2ce518e4f6fada57727f0abe72d.efi", ".cmdline")))
# systemd-stub (UKI section name)
tpm.extend(EventIPL(
    pcr=11,
    data=EventDataString(".initrd\0", "ascii")))
# systemd-stub (UKI section content)
tpm.extend(EventIPL(
    pcr=11,
    data=EventDataPEImageSection("/boot/efi/EFI/Linux/linux-6.1.0-0.rc5.39.fc38.x86_64-ed41c2ce518e4f6fada57727f0abe72d.efi", ".initrd")))

# Linux (cmdline received)
tpm.extend(EventEventTag(
    pcr=9,
    data=EventDataPEImageSection("/boot/efi/EFI/Linux/linux-6.1.0-0.rc5.39.fc38.x86_64-ed41c2ce518e4f6fada57727f0abe72d.efi", ".cmdline")))
# Linux (initrd received)
tpm.extend(EventEventTag(
    pcr=9,
    data=EventDataPEImageSection("/boot/efi/EFI/Linux/linux-6.1.0-0.rc5.39.fc38.x86_64-ed41c2ce518e4f6fada57727f0abe72d.efi", ".initrd")))

# EDK2
tpm.extend(EventEFIAction(
    pcr=5,
    strval="Exit Boot Services Invocation"))
# EDK2
tpm.extend(EventEFIAction(
    pcr=5,
    strval="Exit Boot Services Returned with Success"))

print ("PCRS\n")
print (tpm)

print ("Log\n")
print (tpm.log)

host = TPM.from_host()
host.compare(tpm)
