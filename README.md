  TPM Prevision
  =============

The goal of this package is to demonstrate how to have a prevision of the
PCR values that will result from booting a machine with the TPM.

Various pieces of informationm are needed to have a prevision:

 * The SecureBoot certificate lists from the firmware
      virt-fw-vars --export can provide these

 * Any Option ROMs that insert themselves in the boot path
     Notably iPXE

 * The certificate vendor DB built into shim

 * The EFI binaries for the bootloader(s) and Linux

 * The GPT partition table

 * The firmware boot entries and order

The 'precog.py' application determines a prevision of the TPM
state from a few metadata files.

The guest disk image needs to be exposed as either a raw block
device or raw file. If the disk image is qcow2 this means that
qemu-nbd should be used to turn it into a raw block device. The
disk image should also be mounted

  $ qemu-nbd -c /dev/nbd0 /path/to/disk/image.qcow2
  $ guestmount -i -a /dev/nbd0 /mnt
  $ precog.py \
      --image-manifest examples/image.yaml \
      --firmware-manifest examples/ovmf-manifest.yml \
      --image-file /dev/nbd0 \
      --image-mount /mnt 

The two manifest files reference various paths and will need
to be customized to match the image contents.

By default it will print out the TPM PCRs. The event log
can be requested using '--event-log'. The '--debug' flag
can be used to show intermediate PCR states, which can be
useful for matching with OVMF debug output.

This code is licensed under the LGPL version 2.1 or later
